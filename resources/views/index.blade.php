@extends('studentlayouts.mainlayout')

<?php
$stdmt = 'active';
?>

@section('maincontainer')

<div class="mdk-drawer-layout mdk-js-drawer-layout" push has-scrolling-region style="margin-top: 35px;">
    <div class="mdk-drawer-layout__content">
        <div class="container-fluid">

            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="/othercourses">Other Courses</a></li>
                <li class="breadcrumb-item active">{{$course->course_name}}</li>

            </ol>


            <div class="row">
                <div class="col-lg-8">
                   
                    <div class="card">
                        <div class="card-header bg-numberone">
                            <div class="media">
                                <div class="media-body media-middle">
                                    <h4 class="card-title" style="font-size: 17px;">{{$course->course_name}} Course Purchase</h4>
                                </div>
                            </div>
                        </div>

                        <center>
                            <strong>Course Description</strong><br><hr>

                            @if (is_null($course->description))
                                <p>No description available for the course.</p>
                            @else
                                <p>{{$course->description}}</p>
                            @endif

                            <hr>

                            <p><strong>Course Price : </strong>Rs. {{$course->course_config->course_price}}</p>

                            @if (is_null($course->course_config->publish_date))
                            <p><strong>Course Duration : </strong> {{$course->course_duration}} days</p>
                            @else
                            <p><strong>Course Duration : </strong> Course Expires on {{date_format(date_create($course->course_config->publish_date), "F j Y")}}</p>
                            @endif

                            <a href="#" class="btn btn-success">Pay Now</a><br>
                        </center>
                        
                    </div>

                </div> 



                <div class="col-lg-4">
                    @if(in_array('news_mod', $app_permission))
                        <div class="card">
                        <div class="card-header bg-numberone">
                            <div class="media">
                                <div class="media-body media-middle">
                                    <h4 class="card-title" style="font-size: 17px;">Notice Section</h4>
                                </div>

                            </div>
                        </div>
                        <ul class="list-group list-group-fit" style=" overflow-y: scroll; font-size: 14px; height: 130px;">

                            @forelse($latest_news as $news)
                            <li class="list-group-item right-side-items forum-thread media">

                                <div class="media-body media-middle">
                                    <a href="/news-description/{{$news->id}}">{{$news->news_title}}</a>
                                </div>
                            </li>
                            @empty
                            <li class="list-group-item right-side-items forum-thread media  text-xs-center">

                                <div class="media-body media-middle" style="font-style: italic;">
                                    No Latest News!
                                </div>
                            </li>

                            @endforelse
                        </ul>
                    </div>
                    @endif

                    <div class="card">
                        <div class="card-header bg-numberone">
                            <div class="media">
                                <div class="media-body media-middle">
                                    <h4 class="card-title" style="font-size: 17px;">Course Updates</h4>
                                </div>

                            </div>
                        </div>
                        <ul class="list-group list-group-fit" style=" overflow-y: scroll; font-size: 14px; height: 260px;">

                            @forelse($course_updates as $new_update)

                            <li class="list-group-item right-side-items forum-thread media">

                                <div class="media-body media-middle">
                                    {!! $new_update->data['data'] !!}
                                </div>
                            </li>
                            @empty
                            <li class="list-group-item right-side-items forum-thread media  text-xs-center">

                                <div class="media-body media-middle">
                                    No update
                                </div>
                            </li>
                            @endforelse
                        </ul>
                    </div>

                </div>
            </div>

        </div>
    </div>

    @endsection