
<!DOCTYPE html>
<html>
<head>

<title>Payment Processing</title>
	<script>
    function submitForm() {
      var postForm = document.forms.postForm;
      postForm.submit();
    }
</script>
</head>
<body onload="submitForm();">
<center>
	<div>
	<h2>Payment Processing</h2>
	<table>
	<tr><td>Transaction Id</td><td><strong>{{$txnid}}</strong></td><td>Amount: </td><td><strong>Rs. {{$amount}}</strong></td></tr>
	</table>
	<div >
	<p>Please be patient. this process might take some time,<br />please do not hit refresh or browser back button or close this window</p>
	</div>
	</div>

	<div>
		<form name="postForm" action="https://www.feepal.org/erp-exam-module" method="POST" >
			{{csrf_field()}}
			<input type="hidden" name="CHECKSUM" value="{{$hash}}"/>
			<input type="hidden" name="TRANSACTION_NO" value="{{$txnid}}" />
			<input type="hidden" name="AMOUNT" value="{{$amount}}" />
			<input type="hidden" name="FULLNAME" value="{{$firstname}}" />
			<input type="hidden" name="EMAIL" value="{{$email}}" />
			<input type="hidden" name="PHONENO" value="{{$phone}}" />
			<input type="hidden" name="FID" value="{{$fid}}" />
			<input type="hidden" name="USER_ID" value="{{$user_id}}"/>
			<input type="hidden" name="ERP" value="{{$erp}}" />
		</form>
	</div>
</center>
</body>
</html>

