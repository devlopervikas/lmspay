<!DOCTYPE html>
<html>
<head>
</head>
<body style="text-align: center;">
	<div>
		<h2>Payment Failure</h2>
	</div>

	<div>
		@if ($status)
			@if ($status == 'failure')
				<p>Payment Failed.Details are below</p>
				<p>Failure Reason: {{$unmappedstatus}}</p>
				<p>Txn Id: {{$txnid}}</p>
				<p>Name: {{$firstname}}</p>
				<p>Email: {{$email}}</p>
				<p>Amount: {{$amount}}</p>
				<p>Phone No: {{$phone}}</p>
				<p>Product Info: {{$productinfo}}</p>
			@endif
		@endif
	</div>

	<form name="postForm" action="/" id="payment_proceed" class="form-horizontal" role="form" method="Get" >
	        
        <center><input type="submit" class="btn btn-success" value="Try Again"></center>
    </form><br>
    
    <a href="/">Go Back</a>
</body>
</html>

