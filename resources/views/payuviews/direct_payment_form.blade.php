@extends('layouts.userlayout')

@section('title')
  <title>Payment</title>
@endsection
@section('maincontainer')

 <style type="text/css">
    
    .registerhide {
        display: none;
    }
    .loginhide {
        display: none;
    }
   
  </style>

<div class="row" style="background-image: url('assets/img/logo/op-20.jpg); background-repeat: no-repeat; background-size: cover; margin: 0; height: 130vh;">
  <div class="col-s-size: cover; marm-8 push-sm-1 col-md-4 push-md-4 col-lg-4 push-lg-4" style="background-color: {{$vendor->login_form_color}}; border-radius: 5px; 
          margin-top: 25px;">
      <div class="text-xs-center m-2">
       @php
        $style = "height:".$vendor->application_logo_height.";width:".$vendor->application_logo_width.";";
        @endphp
      <img style="{{$style}}" src="{{$vendor->application_logo}}"><br><br>       
    </div>
    
    <div class="card bg-transparent" style="border : none;">

      <div class="p-2 " id="loginform">
         <div class="card-header bg-white text-xs-center">
        <h4 class="card-title">Payment</h4>
        <p class="card-subtitle">Make Payment</p>
        <div class="" id="msg_div">

              <span id="res_message" style="color: green;font-size: 22px;"></span>

      </div>

        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        
	@if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        
      </div>
      <br/>
        <form role="form" method="POST" action="/direct/process">
            {{ csrf_field() }}
            <input type="hidden" name="vendor" value="{{ $vendor->vendor_id }}" >
            
            <div class="form-group">
            <input type="text" class="form-control" name="fullname" value="{{ old('fullname') }}" placeholder="Enter your name" >

                @if ($errors->has('fullname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('fullname') }}</strong>
                    </span>
                @endif
          </div>
            
          <div class="form-group">
            <input type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Enter your email address">

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
          </div>
            
            <div class="form-group">
            <input type="number" class="form-control" name="mobile" value="{{ old('mobile') }}" placeholder="Enter your mobile number">

                @if ($errors->has('mobile'))
                    <span class="help-block">
                        <strong>{{ $errors->first('mobile') }}</strong>
                    </span>
                @endif
          </div>
            
            <div class="form-group">
            <input type="number" class="form-control" name="amount" value="{{ old('amount') }}" placeholder="Enter amount" >

                @if ($errors->has('amount'))
                    <span class="help-block">
                        <strong>{{ $errors->first('amount') }}</strong>
                    </span>
                @endif
          </div>
            
            <div class="form-group">
                
                <textarea name="remark" class="form-control" placeholder="Please specify the payment reason">{{ old('remark') }}</textarea>
            
                @if ($errors->has('remark'))
                    <span class="help-block">
                        <strong>{{ $errors->first('remark') }}</strong>
                    </span>
                @endif
          </div>
            
            <div class="form-group">
                
                <select class="form-control" name="currency" required="">
                    <option value="">Choose Currency</option>
                    <option value="INR">INR</option>
                    <option value="USD">USD</option>
                </select>
                
            
                @if ($errors->has('currency'))
                    <span class="help-block">
                        <strong>{{ $errors->first('currency') }}</strong>
                    </span>
                @endif
          </div>
          
          <div class="form-group ">
            <button type="submit" class="btn  btn-primary btn-block">
              <span class="btn-block-text">Pay NOw</span>
            </button>
          </div>
            
        
        </form>
      
    
    </div>
    
  </div>
</div>
</div>

@endsection
 