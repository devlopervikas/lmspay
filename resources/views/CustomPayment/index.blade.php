@extends('layouts.master')

@section('content')
<style type="text/css">
    .create-course-tab{
        margin-bottom: 1px;
        margin-left: 5px;
    }

    .dataTables_length, .dataTables_filter, .dataTables_info, .dataTables_paginate, .paging_simple_numbers{
        display: none;
    }

    .search-display{
        margin-top: 2%;
    }

    .view-button{
        padding: 2px 4px; 
        margin-left: 4px;
    }

    .delete-button{
        padding: 2px 4px; 
        margin-right:20px;
    }

    .pagination{
        margin-left: 1%;
    }
    .btnsize
    {
        width: 38px;
        margin-bottom: 5px;
    }
    @media screen and (max-width: 400px){
        .view-button{
            padding: 0px 1px;
            margin-left: 4px;
        }

        .delete-button{
            padding: 0px 4px;
            margin-right: 16px;
        }
    }
</style>
<div class="container" style="width: 100%">
    <div class="row">
        <div class="col-xs-12 col-md-12" style="margin-top: 10px">



            <div class="box box-info search-display" style="border-top-color:#3C8DBC ">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                    <i class="fa fa-search"></i>
                    <h3 class="box-title">Quick Search Class</h3>
                </div>
                <div class="box-body">



                    <form action="/custom-payment" method="get">
                        <div class="form-group col-md-4">
                            <input type="text" class="form-control" name="c" value="{{ isset($_GET['c']) ? $_GET['c'] : '' }}" placeholder="Search using Transaction Id">
                        </div>
                        <div class="form-group col-md-4 buttons-mobile-view">
                            <button  class="btn btn-success" id="search">Search <i class="fa fa-arrow-circle-right"></i>
                            </button>
                            <a href="/custom-payment" class="btn btn-primary" id="reset">Reset<i class="fa fa-refresh" style="margin-left: 5px;"></i>
                            </a>
                        </div>
                    </form>


                </div>
            </div>



            <div class="box box-info courselist-display" style="border-top-color:#3C8DBC ">
                <div class="box-header with-border" >
                    <h3 class="box-title">Transactions</h3>
                         
                    <div class="box-tools pull-right" style="display: none;"> 
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                @php
                $col_check = false;
                @endphp
                <div class="box-body" style="overflow-x: auto;">
                    <table class="table table-bordered table-striped table-hover" style="font-size: 14px;">
                        <thead>
                            <tr>
                                <th>Transaction ID</th>                          
                                <th>Email</th>                          
                                <th>Name</th>                          
                                <th>Mobile</th>                          
                                <th>Amount</th>                          
                                <th>Status</th>                          
                                 
                            </tr>
                        </thead>
                        <tbody>
                            <!--@php $cur_date = date('Y-m-d H:i'); @endphp-->
                            @foreach ($transaction as $me)
                            @php    $cur_date = date('Y-m-d H:i:s');
                            $start_time = $me->start_time;
                            $endTime = date('Y-m-d H:i:s', strtotime( $me->start_time.' +'. $me->duration.' minutes'));
                            @endphp

                            <tr>
                                <td>{{$me->txn_id}}</td> 
                                <td>{{$me->email}}</td> 
                                <td>{{$me->name}}</td> 
                                <td>{{$me->mobile}}</td> 
                                <td>{{$me->amount}}</td> 
                                <td>{{$me->status}}</td> 

                                @if($col_check)
                                <td>
                                    <form id="lcdeleteform" action="/custom-payment/{{ $me->id }}" method="POST">
                                        {{csrf_field()}}
                                        {{method_field('DELETE')}}
                                       
                                          <a href="{{ route('custom-payment.create', $me->id) }}" data-toggle="tooltip" title="Edit Transaction" class="btn btn-success btnsize" >
                                            <i class="fa fa-edit "></i>
                                           </a>
                                       
                                     </form>

                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                     
                </div>


            </div> 
        </div>
    </div>
</div>

<!--modal end-->
@endsection


