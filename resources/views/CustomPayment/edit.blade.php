@extends('layouts.master')

<?php
    $cc = 'active';
?>

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-2" style="margin-top: 10px">
            <div class="panel panel-default">
                <div class="panel-heading">Update S3 bucket setup</div>
                <div class="panel-body">
                    <form action="/s3-bucket/{{$id}}" class="form-horizontal" role="form" method="POST" >
                        {{method_field('PUT')}}
                        {{ csrf_field() }}

                         <div class="form-group{{ $errors->has('course_name') ? ' has-error' : '' }}">
                            <label for="bucket_name" class="col-md-4 control-label">Bucket name *</label>

                            <div class="col-md-6">
                                <input id="bucket_name" type="text" class="form-control" name="bucket_name" value="{{ $bucket_name}}" placeholder="Enter the bucket name" required>

                                @if ($errors->has('bucket_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bucket_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        
                        <div class="form-group{{ $errors->has('bucket_region') ? ' has-error' : '' }}">
                            <label for="bucket_region" class="col-md-4 control-label">Bucket Region *</label>
                            <div class="col-md-6">
                            
                                <input id="duration" type="text" class="form-control" name="bucket_region" 
                                    value="{{ $bucket_region }}" placeholder="Enter a valid region" required>    
                                
                                @if ($errors->has('duration'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bucket_region') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('bucket_key') ? ' has-error' : '' }}">
                            <label for="bucket_key" class="col-md-4 control-label">Bucket Key *</label>
                            <div class="col-md-6">
                            
                                <input id="bucket_key" type="text" class="form-control" name="bucket_key" 
                                    value="{{ $bucket_key }}" placeholder="Enter bucket access key" required>    
                                
                                @if ($errors->has('bucket_key'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bucket_key') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('bucket_secret') ? ' has-error' : '' }}">
                            <label for="bucket_secret" class="col-md-4 control-label">Bucket Secret *</label>
                            <div class="col-md-6">
                            
                                <input type="text" id="bucket_secret" class="form-control" placeholder="Enter bucket secret key"  name="bucket_secret"  value="{{ $bucket_secret }}" required=""> 
                                @if ($errors->has('bucket_secret'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bucket_secret') }}</strong>
                                    </span>
                                @endif
                                
                                
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('bucket_status') ? ' has-error' : '' }}">
                            <label for="bucket_secret" class="col-md-4 control-label">Bucket Status *</label>
                            <div class="col-md-6">
                            
                                <select  id="bucket_status" class="form-control"    name="bucket_status"   required="">
                                <option value="ACTIVE">Active</option> 
                                <option value="DISABLED">Disable</option> 
                            </select>
                                @if ($errors->has('bucket_status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bucket_status') }}</strong>
                                    </span>
                                @endif
                                
                                
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4" style="padding-left: 80px;">
                                <button type="submit" class="btn btn-success" style="padding: 2px 6px;">
                                    Update S3 Bucket
                                </button>
                                <a href="/s3-bucket" class="btn btn-danger" data-toggle="tooltip" title="back to courses" style="padding: 2px 6px;">
                                    Skip
                                </a>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

