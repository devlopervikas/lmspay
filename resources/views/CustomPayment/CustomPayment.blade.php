
<!doctype html>
<html lang="en">
  <head>
    ...
    <link href="/css/app.css" rel="stylesheet">
    
  </head>
  <body>
   
<div class="container" center>
    <div class="row">
        <div class="col-sm-6 col-sm-offset-2" style="margin-top: 10px;margin-left: 254px; ">
            <div class="panel panel-default">
                <div class="panel-heading">Pay Amount</div>
                <div class="panel-body">
                    <form action="/custom-payment" class="form-horizontal" role="form" method="POST" >
                        {{ csrf_field() }}
                        <input type="hidden" name="vendor_id" value="{{ $vendor_id}}" >

                         <div class="form-group{{ $errors->has('course_name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">name *</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Enter the name" required>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Email *</label>
                            <div class="col-md-6">
                            
                                <input id="duration" type="text" class="form-control" name="email" 
                                    value="{{ old('email') }}" placeholder="Enter a valid Email" required>    
                                
                                @if ($errors->has('duration'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            <label for="mobile" class="col-md-4 control-label">Mobile No *</label>
                            <div class="col-md-6">
                            
                                <input id="mobile" type="text" class="form-control" name="mobile" 
                                    value="{{ old('mobile') }}" placeholder="Enter Mobile No" required>    
                                
                                @if ($errors->has('bucket_key'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                            <label for="amount" class="col-md-4 control-label">Amount *</label>
                            <div class="col-md-6">
                            
                                <input type="text" id="amount" class="form-control" placeholder="Enter Amount"  name="amount"  value="{{ old('structure') }}" required=""> 
                                @if ($errors->has('bucket_secret'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                @endif
                                
                                
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Description *</label>
                            <div class="col-md-6">
                            
                                <input type="text" id="description" class="form-control" placeholder="Enter Description"  name="description"  value="{{ old('structure') }}" required=""> 
                                @if ($errors->has('bucket_secret'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                                
                                
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4" style="padding-left: 80px;">
                                <button type="submit" class="btn btn-success" style="padding: 2px 6px;">
                                    Pay
                                </button>
                                <a href="/custom-payment" class="btn btn-danger" data-toggle="tooltip" title="back to courses" style="padding: 2px 6px;">
                                    Skip
                                </a>
                            </div>
                        </div>
                    </form>
                  @if(Session::has('message'))
                    <p class="alert alert-info">{{ Session::get('message') }}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
    <script src="/js/app.js"></script>
  </body>
</html> 
