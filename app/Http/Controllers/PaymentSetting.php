<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Transaction;


class PaymentSetting extends Controller
{
   public function index() {
 
        $txn_id = isset($request->c) ? $request->c : NULL;

        $user = Auth::user();
        $cur_date = date('Y-m-d');
        $vendorID = 68;
        if(!empty($user))
        $vendorID = $user->vendor_id = 68;
        $transaction = Transaction::when($txn_id, function ($q, $txn_id) {
                            return $q->where('txn_id', 'like', '%' . $txn_id . '%');
                        })
                        ->orderBy('created_at', 'DESC')->paginate(10);
 
       return view('CustomPayment.index', compact('transaction'));
    }
    
    public function create(){
     return view('CustomPayment.CustomPayment', ['vendor_id' => 68/*Auth::user()->vendor_id*/]);
    }
    
    public function store(Request $request)
    {
         
        try{

        if ($request->api_token) {
            $user = Auth::guard('api')->user();
            $base_url = URL::to('/').'/api';
        }else {
            $user = Auth::user();
            $base_url = URL::to('/');
              
        }
        
        $vendor_id = 68;//$user->vendor_id;
        
 
        $firstname = $request->name;
        $email = $user_id = $request->email;
        $amount = 0;
        if(isset($request->amount)){
            $amount = $request->amount;
        }
         
        $phone = $request->mobile;
        $productinfo = $request->description;

        $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 30);
        // $fid = "FPKD1114";
        // $fkey = "KHpEcUS";
        // $erp = "KD_VIDEO";

        //initiate transaction 
        $transaction = new Transaction;
        $transaction->vendor_id = $vendor_id; 
        $transaction->name = $request->name;
        $transaction->description = $productinfo;
        $transaction->mobile = $request->mobile;
        $transaction->amount = $request->amount;
        $transaction->course_id = 1;
        $transaction->email = $email;
        $transaction->status = 'TXN_PENDING';
        $transaction->txn_id = $txnid;
        $transaction->save();
        
        $surl = $base_url."/payment/success";
        $furl = $base_url."/payment/failure";
        $service_provider = "payu_paisa";

             
            // Change the Merchant key here as provided by Payumoney
        $MERCHANT_KEY = 'kda9Tn8L';//$pgateway_credential->merchant_key;//"hczDby4i";
        $MERCHANT_ID = 6365017;
        // Change the Merchant Salt as provided by Payumoney
        $SALT = 'hPjxp2xcmx';//$pgateway_credential->salt_key;//"67amBJVBtK";
       
        
        $hashSequence = "$MERCHANT_KEY|$txnid|$amount|$productinfo|$firstname|$email|||||||||||$SALT";
        $hash = hash("sha512", $hashSequence);

        $data_arr = [
                        'email' => $email,
                        'phone' => $phone,
                        'firstname' => $firstname,
                        'txnid' => $txnid,
                        'amount' => $amount,
                        'surl' => $surl,
                        'furl' => $furl,
                        'service_provider' => $service_provider,
                        'merchant_key' => $MERCHANT_KEY,
                        'merchant_id' => $MERCHANT_ID,
                        'salt' => $SALT,
                        'hash' => $hash,
                        'productinfo' => $productinfo
                    ];

        if ($request->api_token) {
            return $this->sendResponse($data_arr, 'Payment initiated');
        }

        $request->session()->put('transaction_id', $transaction->id);
       // $request->session()->put('material_mode', $request->material_mode);
        
        return view('payuviews.form_process_custom', $data_arr);
            
        }
        catch (Exception $e){
            return back()->with('error', $e->getMessage());
        }
    }
    
    public function paymentfailure(Request $request) {
        
        $txnid = $request->txnid;
        $payuMoneyId = $request->payuMoneyId;
        $firstname = $request->firstname;
        $email = $request->email;
        $amount = $request->amount;
        $phone = $request->phone;
        $productinfo = $request->productinfo;
        $unmappedstatus = $request->unmappedstatus;
        $status = $request->status;
        $udf1 = is_null($request->udf1) ? NULL : $request->udf1 ;

        if ($udf1) {
            $transaction = Transaction::where('txn_id', $txnid)->first();
        }else {
            $transaction_id = $request->session()->get('transaction_id');
            $transaction = Transaction::findOrFail($transaction_id);
        }

        $transaction->payment_id = $payuMoneyId;
        $transaction->status = $status;

        $transaction->update();

        if ($udf1) {
            return $this->sendResponse($txnid, 'Payment failed.');
        }

        $request->session()->forget('material_mode');

        return view('payuviews.failure', [
                                        'txnid' =>$txnid,
                                        'firstname' =>$firstname,
                                        'email' => $email,
                                        'amount' => $amount,
                                        'phone' => $phone,
                                        'productinfo' => $productinfo,
                                        'unmappedstatus' => $unmappedstatus,
                                        'status' => $status,
                                        'id' =>  1
                                       ]);
        Session::flash('message', 'Payment Failure.'); 
          return redirect("/")->with( ['successdata' => 'Payment Failure.'] );
    
    }

    public function paymentsuccess(Request $request) {
        $txnid = $request->txnid;
        $payuMoneyId = $request->payuMoneyId;
        $firstname = $request->firstname;
        $email = $request->email;
        $amount = $request->amount;
        $phone = $request->phone;
        $productinfo = $request->productinfo;
        $unmappedstatus = $request->unmappedstatus;
        $status = $request->status;

        $udf1 = is_null($request->udf1) ? NULL : $request->udf1 ;
        if ($udf1) {
            $transaction = Transaction::where('txn_id', $txnid)->first();
        }else {
            $transaction_id = $request->session()->pull('transaction_id');
            $transaction = Transaction::findOrFail($transaction_id);
        }
        $transaction->payment_id = $payuMoneyId;
        $transaction->status = $status;

        $transaction->update();
        if ($udf1) {
            return $this->sendResponse($txnid, 'Payment successfull.');
        }
        Session::flash('message', 'Payment successfull.'); 

        return redirect("/")->with( ['successdata' => 'Payment successfull.'] );
        
    }
    
}
